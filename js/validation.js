function printError(elemId, hintMsg) {
    document.getElementById(elemId).innerHTML = hintMsg;
}

// Defining a function to validate form 
function validateForm() {
    // Retrieving the values of form elements 
    var fname = document.contactForm.fname.value;
    var lname= document.contactForm.lname.value
    var email = document.contactForm.email.value;
    var mobile = document.contactForm.mobile.value;
    var country = document.contactForm.country.value;
    var gender = document.contactForm.gender.value;
    var date= document.contactForm.date.value;
    var departure = document.contactForm.Ddate.value;
    var Room= document.contactForm.Room.value;
    
	// Defining error variables with a default value
    var FnameErr = LnameErr = emailErr = mobileErr = countryErr = dateErr = DdateErr = roomErr = true;
    
    // Validate Fname
    if(fname == "") {
        printError("FnameErr", "Please enter your first name");
    } else {
        var regex = /^[a-zA-Z\s]+$/;                
        if(regex.test(fname) === false) {
            printError("FnameErr", "Please enter a valid first name");
        } else {
            printError("FnameErr", "");
            FnameErr = false;
        }
    }
    // validate Arrival date
    if(date == "") {
        printError("dateErr", "Arrival date is needed");
    }else{
        dateErr = false;
    }

   // validate departure date
    if(departure == "") {
        printError("DdateErr", "Departure date is needed");
    }else{
        DdateErr = false;
    }

  // validate last name
    if(lname == "") {
        printError("LnameErr", "Please enter your last name");
    } else {
        var regex = /^[a-zA-Z\s]+$/;                
        if(regex.test(lname) === false) {
            printError("LnameErr", "Please enter a valid last name");
        } else {
            printError("LnameErr", "");
            LnameErr = false;
        }
    }
    


    
    // Validate email address
    if(email == "") {
        printError("emailErr", "Please enter your email address");
    } else {
        // Regular expression for basic email validation
        var regex = /^\S+@\S+\.\S+$/;
        if(regex.test(email) === false) {
            printError("emailErr", "Please enter a valid email address");
        } else{
            printError("emailErr", "");
            emailErr = false;
        }
    }
    
    // Validate mobile number
    if(mobile == "") {
        printError("mobileErr", "Please enter your mobile number");
    } else {
        var regex = /^[1-9]\d{9}$/;
        if(regex.test(mobile) === false) {
            printError("mobileErr", "Please enter a valid 10 digit mobile number and cant start with 0");
        } else{
            printError("mobileErr", "");
            mobileErr = false;
        }
    }
    
    // Validate country
    if(country == "Select") {
        printError("countryErr", "Please select your country");
    } else {
        printError("countryErr", "");
        countryErr = false;
    }
     // validate room
    if(Room == "Select") {
        printError("roomErr", "Please select a Room");
    } else {
        printError("roomErr", "");
        roomErr = false;
    }
    
    // Validate gender
    if(gender == "") {
        printError("genderErr", "Please select your gender");
    } else {
        printError("genderErr", "");
        genderErr = false;
    }
    
    // Prevent the form from being submitted if there are any errors
    if((FnameErr || LnameErr || emailErr || mobileErr || countryErr || dateErr || DdateErr || roomErr) == true) {
       return false;
    } else {
        // Creating a string from input data for preview
        var dataPreview = "You've entered the following details: \n" +
                          "First Name: " + fname + "\n" + 
                          "Last Name: " + lname + "\n" + 
                          "Email Address: " + email + "\n" +
                          "Mobile Number: " + mobile + "\n" +
                          "Country: " + country + "\n" +
                          "Gender: " + gender + "\n" +
                          "Arrival: " + date + "\n" +
                          "Departure: " + departure + "\n" +
                          "No.of rooms: " + Room + "\n";
       
        // Display input data in a dialog box before submitting the form
        alert(dataPreview);
    }
};